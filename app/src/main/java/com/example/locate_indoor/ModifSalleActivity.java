package com.example.locate_indoor;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.locate_indoor.Client.AsyncHttpClientBuilder;
import com.example.locate_indoor.Client.HeadersManager;
import com.example.locate_indoor.adapters.SalleAdapter;
import com.example.locate_indoor.objets.Salle;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHeaders;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ModifSalleActivity  extends AppCompatActivity {

    private ArrayList<Salle> salles = new ArrayList<>();
    private long idCurrent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifsalle);
        getSalles();
    }

    public void getSalles(){

        TextView error = (TextView) findViewById(R.id.error);
        String url = getString(R.string.base_url) + getString(R.string.salle_url);
        AsyncHttpClient client = new AsyncHttpClientBuilder()
                .build();
        client.get(url, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                switch (statusCode) {
                    case HttpURLConnection.HTTP_OK:
                        salles.clear();
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject j = response.getJSONObject(i);
                                salles.add(convertSalle(j));
                            } catch (JSONException e) {
                                error.setText(getApplicationContext().getString(R.string.errorSalles));
                            }
                        }

                        ListView listView = (ListView) findViewById(R.id.listeSallesModifiables);
                        SalleAdapter adapter;
                        adapter = new SalleAdapter(getApplicationContext(), salles);
                        listView.setAdapter(adapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                System.out.println("TU AS CLIQUE A LA POSITION " + position + " BRAVO GG A TOI");
                                findViewById(R.id.modifLayout).setVisibility(View.VISIBLE);
                                Salle salle=salles.get(position);
                                idCurrent = salle.getId();
                            }
                        });
                        break;
                    case HttpURLConnection.HTTP_FORBIDDEN:
                        error.setText(getApplicationContext().getString(R.string.errorSalles));
                        break;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                System.out.println("On Failure with code " + statusCode);
                error.setText(getApplicationContext().getString(R.string.errorSalles));
            }
        });
    }

    public Salle convertSalle(JSONObject obj) throws JSONException {
        int id = obj.getInt("id");
        int numero = obj.getInt("numero");
        String nom = obj.getString("nom");
        Salle salle = new Salle(id, null, nom, numero, null, null);
        return salle;
    }

    public void clickModif(View view) {
        System.out.println(idCurrent + "    ID COURRENT ICI LA ICI");
        EditText newName = (EditText) findViewById(R.id.nouveauNom);
        //TextView error = (TextView) findViewById(R.id.error);
        //error.setText("");

        if (TextUtils.isEmpty(newName.getText().toString())) {
            newName.setError(getString(R.string.errorTextEmpty));
            return;
        }
        String url = getString(R.string.base_url) + getString(R.string.sallemodif_url) + String.valueOf(idCurrent);
        //System.out.println(url + " ICICICICICIC");
        StringEntity jsonEntity = null;
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("nom", newName.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            jsonEntity = new StringEntity(jsonParams.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        AsyncHttpClient client = new AsyncHttpClientBuilder()
                .addHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .build();

        client.put(this, url, jsonEntity, "application/json", new JsonHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                System.out.println("On Failure with code " + statusCode);
                //error.setText(getString(R.string.errorModif));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                //TODO
                final HeadersManager headersManager = HeadersManager.build(headers);
                switch (statusCode) {
                    case HttpURLConnection.HTTP_OK:
                        idCurrent=10000;
                        ((EditText) findViewById(R.id.nouveauNom)).setText(" ");
                        findViewById(R.id.modifLayout).setVisibility(View.INVISIBLE);
                        getSalles();
                        break;
                    case HttpURLConnection.HTTP_NOT_FOUND:
                        //error.setText(getString(R.string.errorModif));
                        findViewById(R.id.modifLayout).setVisibility(View.INVISIBLE);
                        break;
                }
            }
        });
    }
}
