package com.example.locate_indoor;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.locate_indoor.Client.AsyncHttpClientBuilder;
import com.example.locate_indoor.adapters.SalleAdapter;
import com.example.locate_indoor.objets.Categorie;
import com.example.locate_indoor.objets.Etage;
import com.example.locate_indoor.objets.Salle;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class DestinationActivity extends AppCompatActivity {

    private ArrayList<Salle> salles = new ArrayList<>();
    private String qrCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            qrCode = extras.getString("QrCode");
        }

        TextView error = (TextView) findViewById(R.id.error);
        String url = getString(R.string.base_url) + getString(R.string.salle_url);
        AsyncHttpClient client = new AsyncHttpClientBuilder()
                .build();
        client.get(url, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                switch (statusCode) {
                    case HttpURLConnection.HTTP_OK :
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject j = response.getJSONObject(i);
                                salles.add(convertSalle(j));
                            } catch (JSONException e) {
                                error.setText(getApplicationContext().getString(R.string.errorSalles));
                            }
                        }

                        SalleAdapter adapter = new SalleAdapter(getApplicationContext(), salles);
                        ListView listView = (ListView) findViewById(R.id.listeSalles);
                        listView.setAdapter(adapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                goToPlan(position);
                            }
                        });
                        break;
                    case HttpURLConnection.HTTP_FORBIDDEN:
                        error.setText(getApplicationContext().getString(R.string.errorSalles));
                        break;
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                System.out.println("On Failure with code " + statusCode);
                error.setText(getApplicationContext().getString(R.string.errorSalles));
            }
        });
    }

    public void goToPlan(int position){
        Intent plan = new Intent(getApplicationContext(), PlanActivity.class);
        Salle s = salles.get(position);
        plan.putExtra("salleId", s.getId());
        plan.putExtra("chemin", "ischemin");
        setResult(  PlanActivity.VALID_DESTINATION_ACTIVITY_RESPONSE, plan );
        finish();
    }

    public Salle convertSalle(JSONObject obj) throws JSONException {
        int id = obj.getInt("id");
        int numero = obj.getInt("numero");
        String nom = obj.getString("nom");
        Salle salle = new Salle(id, null, nom, numero, null,null);
        return salle;
    }
}
