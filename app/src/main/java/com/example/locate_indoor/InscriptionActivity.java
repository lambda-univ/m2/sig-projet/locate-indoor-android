package com.example.locate_indoor;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.locate_indoor.Client.AsyncHttpClientBuilder;
import com.example.locate_indoor.Client.HeadersManager;
import com.example.locate_indoor.Client.HttpUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHeaders;
import cz.msebera.android.httpclient.entity.StringEntity;

public class InscriptionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
    }

    public void clickInscription(View view) {
        EditText nom = (EditText) findViewById(R.id.nomEdit);
        EditText prenom = (EditText) findViewById(R.id.prenomEdit);
        EditText login = (EditText) findViewById(R.id.loginEdit);
        EditText password = (EditText) findViewById(R.id.passwordEdit);
        TextView error = (TextView) findViewById(R.id.error);

        if(TextUtils.isEmpty(nom.getText().toString())) {
            nom.setError(getString(R.string.errorTextEmpty));
            return;
        }
        if(TextUtils.isEmpty(prenom.getText().toString())) {
            prenom.setError(getString(R.string.errorTextEmpty));
            return;
        }
        if(TextUtils.isEmpty(login.getText().toString())) {
            login.setError(getString(R.string.errorTextEmpty));
            return;
        }
        if(TextUtils.isEmpty(password.getText().toString())) {
            password.setError(getString(R.string.errorTextEmpty));
            return;
        }
        String url = getString(R.string.base_url) + getString(R.string.user_url);
        StringEntity jsonEntity = null;
        JSONObject jsonParams = new JSONObject();
        try {
            jsonParams.put("nom", nom.getText().toString());
            jsonParams.put("prenom", prenom.getText().toString());
            jsonParams.put("pseudo", login.getText().toString());
            jsonParams.put("motdepasse", password.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            jsonEntity = new StringEntity(jsonParams.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClientBuilder()
                .addHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                .build();

        client.post(this, url, jsonEntity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                switch (statusCode) {
                    case HttpURLConnection.HTTP_CREATED:
                        returnToconnexion();
                        break;
                    case HttpURLConnection.HTTP_BAD_REQUEST:
                        error.setText(R.string.errorInscription);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println(errorResponse.toString());
                System.out.println("On Failure with code " + statusCode);
                error.setText(R.string.errorInscription);
            }
        });
    }

    public void returnToconnexion(){
        Intent connexion = new Intent(getApplicationContext(), ConnexionActivity.class);
        startActivity(connexion);
    }
}
