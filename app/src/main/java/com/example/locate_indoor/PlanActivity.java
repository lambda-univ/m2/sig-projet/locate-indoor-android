package com.example.locate_indoor;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.locate_indoor.Client.AsyncHttpClientBuilder;
import com.example.locate_indoor.Client.HttpUtils;
import com.example.locate_indoor.objets.QrCode;
import com.example.locate_indoor.objets.Salle;
import com.example.locate_indoor.webapp.commands.ActionInterface;
import com.example.locate_indoor.webapp.commands.HttpMethod;
import com.example.locate_indoor.webapp.commands.RequestParametersBuilder;
import com.example.locate_indoor.webapp.commands.RequestWebInterface;
import com.example.locate_indoor.webapp.commands.RequestWebInterfaceBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.mapbox.geojson.Point;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpClient;


public class PlanActivity extends AppCompatActivity {
    private static final int NO_SALLE = 0;
    private static final int NO_QRCODE = 0;

    private static final int DESTINATION_ACTIVITY_REQUEST = 1;
    public static final int VALID_DESTINATION_ACTIVITY_RESPONSE = 1;

    private static final int SCAN_ACTUALISATION_ACTIVITY_REQUEST = 2;
    public static final int HAS_QRCODE_ACTIVITY_RESPONSE = 1;
    public static final int NO_QRCODE_ACTIVITY_RESPONSE = 2;



    private WebView webView;
    private int salleDestId;
    private int qrcodeId;

    private String qrCode;

    private ArrayList<Salle> salles = new ArrayList<>();
    private Point positionDepart;
    private Point positionArrivee;

    //Pour ls tests
    private String fileName = "plan.html";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        salleDestId = NO_SALLE;
        qrcodeId = NO_QRCODE;

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            String chemin = extras.getString("chemin");
            if(chemin != null) {
                if(!chemin.isEmpty()) {
                    itineraireEnCours();
                    salleDestId = extras.getInt("salleId");
                    //Requete chemin avec salleId
                }
            }else{
                pasItineraireEnCours();
            }
            qrCode = extras.getString("QrCode");
            qrcodeId = extras.getInt("qrcodeId");
            System.out.println("Getting QRCode id: " + qrcodeId);

            try {
                getInfo();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        launchWebView();
    }


    public void getInfo() throws JSONException {
        //Position de l'utilisateur
        JSONObject qr = new JSONObject(qrCode);
        positionDepart = Point.fromJson(String.valueOf(qr.getJSONObject("point")));

    }

    public void clickDestination(View view) {
        Intent dest = new Intent(this, DestinationActivity.class);
        startActivityForResult(dest, DESTINATION_ACTIVITY_REQUEST);
    }

    public void clickArretDestination(View view) {
        pasItineraireEnCours();

        salleDestId = NO_SALLE;
        launchWebView();
        // position de l'utilisateur n'est plus connue (il a pu bouger)
        //On affiche un message qui previent que la position n'est plus connue donc on demande à actualiser la position
        TextView error = (TextView) findViewById(R.id.error);
        error.setText(getString(R.string.errorDestination));
    }

    //Est ce qu'on actualise la position via un scan d'un qr code ?
    public void clickActualisePos(View view) {
        TextView error = (TextView) findViewById(R.id.error);
        error.setText("");
        Intent scan = new Intent(this, ScanActivity.class);
        startActivityForResult( scan, SCAN_ACTUALISATION_ACTIVITY_REQUEST );
    }

    public void clickArrivee(View view) {
        // Stop itineraire
        doArriveRequest();

    }

    public void itineraireEnCours(){
        ((View) findViewById(R.id.btnActualisePosition)).setVisibility(View.GONE);
        ((View) findViewById(R.id.btnDestination)).setVisibility(View.GONE);
        ((View) findViewById(R.id.btnArretDestination)).setVisibility(View.VISIBLE);
        ((View) findViewById(R.id.btnArriveDestination)).setVisibility(View.VISIBLE);
    }

    public void pasItineraireEnCours(){
        ((View) findViewById(R.id.btnActualisePosition)).setVisibility(View.VISIBLE);
        ((View) findViewById(R.id.btnDestination)).setVisibility(View.VISIBLE);
        ((View) findViewById(R.id.btnArretDestination)).setVisibility(View.GONE);
        ((View) findViewById(R.id.btnArriveDestination)).setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // binds destination activity result which returns destination salleId
        if ( requestCode == DESTINATION_ACTIVITY_REQUEST && resultCode == VALID_DESTINATION_ACTIVITY_RESPONSE  ) {
            Log.d("Plan", "Updating salle");
            salleDestId = data.getExtras().getInt( "salleId" );
        }

        // binds scan activity result which result qrcode id
        if ( requestCode == SCAN_ACTUALISATION_ACTIVITY_REQUEST && resultCode == HAS_QRCODE_ACTIVITY_RESPONSE ) {
            Log.d("Plan", "Updating qrcode");
            qrcodeId = data.getExtras().getInt("qrcodeId");
        }

        Log.d("Plan", "Updating plan");
        launchWebView();
    }




    public void launchWebView() {

        Log.i("MyApplication", "Creating webview and inflating " + fileName);
        webView = findViewById(R.id.plan);
        WebSettings ws = webView.getSettings();
        ws.setJavaScriptEnabled(true);
        ws.setBlockNetworkLoads(false);
        ws.setAllowContentAccess(true);
        ws.setAllowFileAccess(true);
        ws.setAllowUniversalAccessFromFileURLs(true);
        System.out.println(ws.getAllowContentAccess());
        webView.setWebChromeClient(new WebChromeClient() {
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.d("MyApplication", consoleMessage.message() + " -- From line "
                        + consoleMessage.lineNumber() + " of "
                        + consoleMessage.sourceId());
                return true;
            }
        });

        // builds requests
        final String baseUrl = getString(R.string.base_url);
        RequestWebInterfaceBuilder builder = new RequestWebInterfaceBuilder(this)
                .setActionInterface( ActionInterface.NO_ACTION )
                .addRequest("all_salle", HttpMethod.GET, baseUrl + getString(R.string.salle_url) )
                .addRequest("all_etage", HttpMethod.GET, baseUrl + getString(R.string.etage_url));

        if( qrcodeId != NO_QRCODE ) {
            builder.addRequest("current_qrcode", HttpMethod.GET, baseUrl + getString(R.string.qrcode_id_url) + qrcodeId);
        }

        if ( salleDestId != NO_SALLE && qrcodeId != NO_QRCODE ) {
            // displays option to manage itineraire
            itineraireEnCours();

            // Adds option to display to
            builder
                    .setActionInterface( ActionInterface.DISPLAY_PATH_ACTION )
                    .addRequest( "itineraire", HttpMethod.POST, baseUrl + getString(R.string.itineraire_url),
                            new RequestParametersBuilder()
                                    .addParam("salleId", salleDestId)
                                    .addParam( "qrcodeId", qrcodeId  )
                                    .build()
                    );
        }

        RequestWebInterface requestWebInterface = builder.build();



        webView.addJavascriptInterface(requestWebInterface,"Android");
        webView.getSettings().setDomStorageEnabled(true);
        webView.loadUrl("file:///android_asset/" + fileName);
    }


    /**
     * Do a request at {{base_url}}/itineraire/arrive/{idSalle} to notify API that we're arrived.
     *
     */
    public void doArriveRequest() {
        // access to authentication token
        AuthenticationTokenManager authenticationTokenManager =
                AuthenticationTokenManager.getInstance();
        if ( !authenticationTokenManager.hasAuthenticationToken() ) {
            Log.d("Plan", "No authentication token provided");
        }
        String token = authenticationTokenManager.getAuthenticationToken();

        AsyncHttpClient client = new AsyncHttpClientBuilder()
                .addHeader( "Authorization", token)
                .build();


        // call request
        String baseUrl = getString(R.string.base_url);
        String arriveUrl = baseUrl + getString(R.string.arrive_url) + salleDestId;
        client.post(arriveUrl, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("Plan", "Failure on arrive request: " + statusCode + ": " + responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                qrcodeId = NO_QRCODE;
                doClosestQRCodeRequest(salleDestId);
                salleDestId = NO_SALLE;

                // update web view
                launchWebView();

                // update interface
                pasItineraireEnCours();
            }
        });
    }

    /**
     * Do a request at {{base_url}}//closestQrcode/{idSalle} to get closest QRCode from given salle id
     */
    public void doClosestQRCodeRequest( int salleId ) {
        AsyncHttpClient client = new AsyncHttpClientBuilder()
                .build();

        // call request
        String baseUrl = getString(R.string.base_url);
        String closestQRCodeUrl = baseUrl + getString(R.string.closest_qrcode) + salleId;
        client.get(closestQRCodeUrl, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("Plan", "Failure on arrive request: " + statusCode + ": " + responseString);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                switch (statusCode) {
                    case HttpURLConnection.HTTP_OK:
                        try {
                            qrcodeId = response.getInt("id");
                            // update web view
                            launchWebView();

                            // update interface
                            pasItineraireEnCours();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case HttpURLConnection.HTTP_BAD_REQUEST:
                        Log.d("Plan", "Failure on bad request" );
                }
            }
        });
    }
}