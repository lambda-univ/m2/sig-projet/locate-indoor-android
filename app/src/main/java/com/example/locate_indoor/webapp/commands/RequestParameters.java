package com.example.locate_indoor.webapp.commands;

import java.util.HashMap;
import java.util.Map;

public class RequestParameters {


    private Map<String, Object> params = new HashMap<>();
    private Map<String, Object> headers = new HashMap<>();

    public RequestParameters() {

    }

    public RequestParameters(Map<String, Object> params, Map<String, Object> headers) {
        this.params = params;
        this.headers = headers;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    public static RequestParameters empty() {
        return new RequestParameters();
    }
}
