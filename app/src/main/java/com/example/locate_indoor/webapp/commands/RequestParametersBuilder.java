package com.example.locate_indoor.webapp.commands;

import java.util.HashMap;
import java.util.Map;

public class RequestParametersBuilder {
    private Map<String, Object> params = new HashMap<>();
    private Map<String, Object> headers = new HashMap<>();

    public RequestParametersBuilder addParam(String name, Object value ) {
        this.params.put( name, value );
        return this;
    }

    public RequestParametersBuilder addHeader(String name, Object value ) {
        this.headers.put( name, value );
        return this;
    }

    public RequestParameters build() {
        return new RequestParameters( params, headers );
    }
}
