package com.example.locate_indoor.webapp.commands;

public enum ActionInterface {
    NO_ACTION("no-action"),
    DISPLAY_PATH_ACTION("display-path-action"),
    DISPLAY_CURRENT_POSITION("display-current-position");

    private String actionName;

    ActionInterface(String actionName) {
        this.actionName = actionName;
    }

    public String getActionName() {
        return this.actionName;
    }
}
