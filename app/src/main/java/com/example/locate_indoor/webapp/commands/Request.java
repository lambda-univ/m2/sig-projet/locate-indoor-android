package com.example.locate_indoor.webapp.commands;

import java.util.HashMap;
import java.util.Map;

public class Request {
    private String name;
    private String url;
    private String method;
    private RequestParameters requestParameters;

    public Request( String name, String url, String method ) {
        this.name = name;
        this.url = url;
        this.method = method;
        this.requestParameters = RequestParameters.empty();
    }

    public Request(String name, String url, String method, RequestParameters requestParameters ) {
        this( name, url, method );
        this.requestParameters = requestParameters;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Map<String, Object> getParams() {
        return requestParameters.getParams();
    }

    public Map<String, Object> getHeaders() { return requestParameters.getHeaders(); }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
