package com.example.locate_indoor.webapp.commands;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class RequestWebInterfaceBuilder {
    private Context context;
    private ActionInterface actionInterface;
    private Collection<Request> requests;


    public RequestWebInterfaceBuilder( Context context ) {
        this.context = context;
        this.requests = new ArrayList<>();
    }

    public RequestWebInterfaceBuilder setActionInterface(ActionInterface actionInterface) {
        this.actionInterface = actionInterface;
        return this;
    }

    public RequestWebInterfaceBuilder addRequest( String name, String method, String url) {
        this.requests.add( new Request( name, url, method ) );
        return this;
    }

    public RequestWebInterfaceBuilder addRequest(String name, String method, String url, RequestParameters requestParameters) {
        this.requests.add( new Request( name, url, method, requestParameters ) );
        return this;
    }



    public  RequestWebInterface build() {
        RequestWebInterface request = new RequestWebInterface( context, actionInterface, this.requests );
        return request;
    }
}
