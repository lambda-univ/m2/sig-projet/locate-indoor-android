package com.example.locate_indoor.webapp.commands;

import android.content.Context;
import android.webkit.JavascriptInterface;

import com.example.locate_indoor.webapp.WebAppInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class RequestWebInterface extends WebAppInterface {

    private static final String ACTION_PARAM_NAME = "action";
    private static final String URLS_PARAM_NAME = "urls";
    private static final String URL_PARAM_NAME = "url";
    private static final String REQUEST_PARAMS_NAME = "params";
    private static final String METHOD_PARAM_NAME = "method";
    private static final String HEADER_PARAM_NAME = "headers";

    private ActionInterface actionInterface;
    private Collection<Request> requests;

    public RequestWebInterface(Context context, ActionInterface actionInterface, Collection<Request > requests ) {
        super(context);
        this.actionInterface = actionInterface;
        this.requests = requests;
    }


    @JavascriptInterface
    public String request() throws JSONException {
        // builds JSON requests
        JSONObject jsonRequests = new JSONObject();
        for ( Request request : this.requests ) {
            // build params object
            JSONObject params = new JSONObject();
            for ( Map.Entry<String, Object> paramEntry : request.getParams().entrySet() )   {
                params.put( paramEntry.getKey(), paramEntry.getValue() );
            }

            // build headers object
            JSONObject headers = new JSONObject();
            for ( Map.Entry<String, Object> headerEntry : request.getHeaders().entrySet() )   {
                headers.put( headerEntry.getKey(), headerEntry.getValue() );
            }

            // build request object
            JSONObject jsonRequest = new JSONObject();
            jsonRequest
                    .put( URL_PARAM_NAME, request.getUrl() )
                    .put( METHOD_PARAM_NAME, request.getMethod() )
                    .put( HEADER_PARAM_NAME, headers )
                    .put( REQUEST_PARAMS_NAME, params );
            jsonRequests.put( request.getName(), jsonRequest );
        }

        // build JSON object with url and params
        JSONObject request = new JSONObject();
        request.put(ACTION_PARAM_NAME, this.actionInterface.getActionName());
        request.put( URLS_PARAM_NAME, jsonRequests );
        return request.toString();
    }

}
