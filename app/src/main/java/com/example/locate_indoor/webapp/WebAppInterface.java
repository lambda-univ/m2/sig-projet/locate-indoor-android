package com.example.locate_indoor.webapp;

import android.content.Context;
import android.webkit.JavascriptInterface;

public class WebAppInterface
{
    Context mContext;

    public WebAppInterface(Context c)
    {
        mContext = c;
    }

    @JavascriptInterface
    public String test()
    {
        return "http://localhost:8080/salle";
    }
}
