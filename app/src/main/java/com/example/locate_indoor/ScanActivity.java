package com.example.locate_indoor;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.locate_indoor.Client.AsyncHttpClientBuilder;
import com.example.locate_indoor.Client.HeadersManager;
import com.example.locate_indoor.Client.HttpUtils;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHeaders;

public class ScanActivity extends AppCompatActivity {

    SurfaceView surfaceView;
    CameraSource cameraSource;
    TextView textView;
    TextView errorText;
    BarcodeDetector barcodeDetector;
    String scanned;
    Boolean chargePlan=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        surfaceView = (SurfaceView) findViewById(R.id.camerapreview);
        textView = (TextView) findViewById(R.id.textView);
        errorText = (TextView) findViewById(R.id.errorScan);

        barcodeDetector = new BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.QR_CODE).build();

        cameraSource = new CameraSource.Builder(this, barcodeDetector).setRequestedPreviewSize(640, 480).build();

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(@NonNull SurfaceHolder surfaceHolder) {
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                try {
                    cameraSource.start(surfaceHolder);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(@NonNull SurfaceHolder surfaceHolder, int i, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(@NonNull SurfaceHolder surfaceHolder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                SparseArray<Barcode> qrCode = detections.getDetectedItems();

                if(qrCode.size()!=0){
                    errorText.setVisibility(View.GONE);
                    textView.post(new Runnable() {
                        @Override
                        public void run() {
                            Vibrator vibrator=(Vibrator)getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(80);
                            scanned=qrCode.valueAt(0).displayValue;

                            if(!chargePlan) {
                                try {
                                    int idCode = Integer.parseInt(scanned);
                                    if (0 < idCode) {
                                        chargePlan = true;
                                        goToPlan(idCode);
                                    } else {
                                        errorText.setVisibility(View.VISIBLE);
                                    }
                                } catch (Exception e) {
                                    errorText.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    public void goToPlan(int qrCode){
        String url = getString(R.string.base_url) + getString(R.string.qrcode_id_url) + qrCode;
        AsyncHttpClient client = new AsyncHttpClientBuilder()
                .build();
        client.get(url, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if (statusCode == HttpURLConnection.HTTP_OK) {
                    if ( calledForResult() ) {
                        Intent plan = new Intent();
                        plan.putExtra("qrcodeId", qrCode);
                        setResult( PlanActivity.HAS_QRCODE_ACTIVITY_RESPONSE, plan );
                        finish();
                    } else {
                        Intent plan = new Intent(getApplicationContext(), PlanActivity.class);
                        plan.putExtra("QrCode", response.toString());
                        plan.putExtra("qrcodeId", qrCode);
                        System.out.println("Scanned QRCode id: " + qrCode);
                        startActivity(plan);
                    }

                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println("On Failure with code " + statusCode);
            }
        });
    }

    private boolean calledForResult() {
        return getCallingActivity() != null;
    }
}
