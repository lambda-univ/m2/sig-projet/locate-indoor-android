package com.example.locate_indoor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.locate_indoor.R;
import com.example.locate_indoor.objets.Salle;

import java.util.ArrayList;

public class SalleAdapter extends ArrayAdapter<Salle> {

    public SalleAdapter(Context context, ArrayList<Salle> salles) {
        super(context, 0, salles);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Salle salle = getItem(position);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_salle, parent, false);
        }

        TextView salleNom = (TextView) convertView.findViewById(R.id.salleNom);
        salleNom.setText(salle.getNom());

        return convertView;
    }
}
