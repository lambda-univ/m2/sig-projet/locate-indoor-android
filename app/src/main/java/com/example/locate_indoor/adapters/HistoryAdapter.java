package com.example.locate_indoor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.locate_indoor.R;
import com.example.locate_indoor.objets.Utilisateur;

import java.util.ArrayList;

public class HistoryAdapter extends ArrayAdapter<Utilisateur> {
    public HistoryAdapter(Context context, ArrayList<Utilisateur> utilisateurs) {
        super(context, 0, utilisateurs);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Utilisateur utilisateur = getItem(position);

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_history, parent, false);
        }

        TextView userInfo = (TextView) convertView.findViewById(R.id.userInfo);
        userInfo.setText(utilisateur.getPrenom()+" "+utilisateur.getNom());
        TextView userLogin = (TextView) convertView.findViewById(R.id.userLogin);
        userLogin.setText(getContext().getText(R.string.Userlogin) + " " + utilisateur.getPseudo());

        return convertView;
    }
}

