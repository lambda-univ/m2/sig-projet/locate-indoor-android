package com.example.locate_indoor;

public class AuthenticationTokenManager {
    private static AuthenticationTokenManager instance;

    public static AuthenticationTokenManager getInstance() {
        if ( instance == null ) {
            instance = new AuthenticationTokenManager();
        }
        return instance;
    }


    private String authenticationToken;

    public boolean hasAuthenticationToken() {
        return this.authenticationToken != null;
    }

    public void setAuthenticationToken( String authenticationToken ) {
        this.authenticationToken = authenticationToken;
    }

    public String getAuthenticationToken() {
        return this.authenticationToken;
    }
}
