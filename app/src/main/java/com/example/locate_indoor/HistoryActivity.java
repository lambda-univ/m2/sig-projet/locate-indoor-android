package com.example.locate_indoor;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.locate_indoor.Client.AsyncHttpClientBuilder;
import com.example.locate_indoor.adapters.HistoryAdapter;
import com.example.locate_indoor.objets.Historique;
import com.example.locate_indoor.objets.Utilisateur;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class HistoryActivity extends AppCompatActivity {

    private ArrayList<Utilisateur> utilisateurs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        TextView error = (TextView) findViewById(R.id.error);

        String url = getString(R.string.base_url) + getString(R.string.users_url);
        AsyncHttpClient client = new AsyncHttpClientBuilder()
                .build();
        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                switch (statusCode) {
                    case HttpURLConnection.HTTP_OK :
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject j = response.getJSONObject(i);
                                utilisateurs.add(new Utilisateur(j));
                            } catch (JSONException e) {
                                error.setText(getApplicationContext().getString(R.string.errorHistory));
                            }
                        }

                        HistoryAdapter adapter = new HistoryAdapter(getApplicationContext(), utilisateurs);
                        ListView listView = (ListView) findViewById(R.id.listeHistory);
                        listView.setAdapter(adapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                AffichageHistorique(position);
                            }
                        });
                        break;
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                System.out.println("On Failure with code " + statusCode);
                error.setText(getApplicationContext().getString(R.string.errorHistory));
            }
        });

    }

    private void AffichageHistorique(int position) {
        Utilisateur user = utilisateurs.get(position);
        TextView error = (TextView) findViewById(R.id.error);
        String url = getString(R.string.base_url) + getString(R.string.historique_user_url) +user.getPseudo();
        AsyncHttpClient client = new AsyncHttpClientBuilder()
                .build();
        client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                switch (statusCode) {
                    case HttpURLConnection.HTTP_OK :
                        try {
                            Historique histo = new Historique(response);
                            HistoryFragment history = (HistoryFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_history);
                            history.set_date(histo.getDate());
                            history.set_lieu(histo.getLieu());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case HttpURLConnection.HTTP_NOT_FOUND:
                        error.setText(getApplicationContext().getString(R.string.errorHistory));
                        break;
                    case HttpURLConnection.HTTP_NO_CONTENT:
                        Historique histo = new Historique(getApplicationContext().getString(R.string.nodateHistory), getApplicationContext().getString(R.string.nolieuHistory));
                        HistoryFragment history = (HistoryFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_history);
                        history.set_date(histo.getDate());
                        history.set_lieu(histo.getLieu());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                System.out.println("On Failure with code " + statusCode);
                error.setText(getApplicationContext().getString(R.string.errorHistory));
            }
        });
    }
}
