package com.example.locate_indoor;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void clickScan(View view){
        startScanActivity();
    }

    public void clickHistorique(View view){
        Intent history = new Intent(getApplicationContext(), HistoryActivity.class);
        startActivity(history);
    }

    public void clickModifSalle(View view){
        Intent modif = new Intent(getApplicationContext(), ModifSalleActivity.class);
        startActivity(modif);
    }

    public void startScanActivity() {
        Intent scan = new Intent(getApplicationContext(), ScanActivity.class);
        startActivity(scan);
    }


}