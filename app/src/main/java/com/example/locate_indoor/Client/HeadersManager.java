package com.example.locate_indoor.Client;

import com.example.locate_indoor.Client.exceptions.HeaderNotfoundException;

import java.util.Collection;

import cz.msebera.android.httpclient.Header;

public class HeadersManager {

    private Header[] headers;

    public HeadersManager(Header[] headers) {
        this.headers = headers;
    }

    public boolean contains( String headerName ) {
        try {
            this.get(headerName);
            return true;
        } catch (HeaderNotfoundException e) {
            return false;
        }
    }

    public String get(String headerName ) throws HeaderNotfoundException {
        for ( Header header : this.headers ) {
            if ( header.getName().equals( headerName ) ) {
                return header.getValue();
            }
        }
        throw new HeaderNotfoundException( "Header with name " + headerName + " not found" );
    }


    public static HeadersManager build( Header[] headers ) {
        return new HeadersManager(headers);
    }
}
