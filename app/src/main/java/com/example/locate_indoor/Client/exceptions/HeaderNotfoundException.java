package com.example.locate_indoor.Client.exceptions;

public class HeaderNotfoundException  extends RuntimeException {
    public HeaderNotfoundException() {
    }

    public HeaderNotfoundException(String message) {
        super(message);
    }
}
