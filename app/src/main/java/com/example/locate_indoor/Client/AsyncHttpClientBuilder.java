package com.example.locate_indoor.Client;

import com.loopj.android.http.AsyncHttpClient;

public class AsyncHttpClientBuilder {
    private AsyncHttpClient client = new AsyncHttpClient();

    public AsyncHttpClientBuilder addHeader( String header, String value ) {
        this.client.addHeader(header, value);
        return this;
    }

    public AsyncHttpClient build() {
        return this.client;
    }
}
