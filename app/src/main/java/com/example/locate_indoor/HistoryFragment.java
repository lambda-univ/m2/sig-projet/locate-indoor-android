package com.example.locate_indoor;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class HistoryFragment extends Fragment {
    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        return view;
    }

    public void set_date(String date) {
        TextView view = (TextView) getView().findViewById(R.id.dateText);
        view.setText(getString(R.string.dateHistory) + " " + date);
    }
    public void set_lieu(String lieu) {
        TextView view = (TextView) getView().findViewById(R.id.lieuText);
        view.setText(getString(R.string.lieuHistory) + " " + lieu);
    }
}
