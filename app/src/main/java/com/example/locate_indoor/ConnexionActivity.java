package com.example.locate_indoor;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.locate_indoor.Client.AsyncHttpClientBuilder;
import com.example.locate_indoor.Client.HeadersManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.net.HttpURLConnection;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpHeaders;

public class ConnexionActivity  extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);

        checkPermission(Manifest.permission.CAMERA,100);
    }

    // Function to check and request permission
    public void checkPermission(String permission, int requestCode)
    {
        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(ConnexionActivity.this, permission) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(ConnexionActivity.this, new String[] { permission }, requestCode);
        }
    }

    public void clickConnection(View view) {
        EditText login = (EditText)findViewById(R.id.loginEdit);
        EditText password = (EditText) findViewById(R.id.passwordEdit);
        TextView error = (TextView) findViewById(R.id.error);
        error.setText("");

        if(TextUtils.isEmpty(login.getText().toString())) {
            login.setError(getString(R.string.errorTextEmpty));
            return;
        }
        if(TextUtils.isEmpty(password.getText().toString())) {
            password.setError(getString(R.string.errorTextEmpty));
            return;
        }
        String url = getString(R.string.base_url) + getString(R.string.login_url);
        RequestParams rp = new RequestParams();
        rp.put("username", login.getText().toString()); rp.put("password", password.getText().toString());
        AsyncHttpClient client = new AsyncHttpClientBuilder()
                .build();

        client.post(url, rp, new TextHttpResponseHandler() {

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                System.out.println("On Failure with code " + statusCode);
                error.setText(getString(R.string.errorConnexion));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                final HeadersManager headersManager = HeadersManager.build(headers);
                switch (statusCode){
                    case HttpURLConnection.HTTP_OK:
                        // retrieve authentication token
                        if ( headersManager.contains( HttpHeaders.AUTHORIZATION ) ) {
                            String token = headersManager.get( HttpHeaders.AUTHORIZATION );

                            // Save authentication token
                            AuthenticationTokenManager authenticationTokenManager =
                                    AuthenticationTokenManager.getInstance();
                            authenticationTokenManager.setAuthenticationToken(token);

                            startMenuActivity();
                        }else{
                            error.setText(getString(R.string.errorConnexion));
                        }
                        break;
                    case HttpURLConnection.HTTP_NOT_FOUND:
                        error.setText(getString(R.string.errorConnexion));
                        break;
                    case HttpURLConnection.HTTP_INTERNAL_ERROR:
                        error.setText(getString(R.string.errorConnexion));
                        break;
                }
            }
        });
    }

    public void startScanActivity() {
        Intent scan = new Intent(getApplicationContext(), ScanActivity.class);
        startActivity(scan);
    }

    public void startMenuActivity() {
        Intent menu = new Intent(getApplicationContext(), MenuActivity.class);
        startActivity(menu);
    }

    public void clickInscription(View view) {
        Intent inscription = new Intent(this, InscriptionActivity.class);
        startActivity(inscription);
    }
}
