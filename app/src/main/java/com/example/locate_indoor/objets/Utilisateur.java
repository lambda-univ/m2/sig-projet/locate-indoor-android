package com.example.locate_indoor.objets;

import com.mapbox.geojson.Geometry;
import com.mapbox.geojson.Polygon;

import org.json.JSONException;
import org.json.JSONObject;

public class Utilisateur {
    private String nom;
    private String prenom;
    private String pseudo;

    public Utilisateur(String nom, String prenom, String pseudo) {
        this.nom = nom;
        this.prenom = prenom;
        this.pseudo = pseudo;
    }

    public Utilisateur() {
    }

    public Utilisateur(JSONObject object) throws JSONException {
        this.nom = object.getString("nom");
        this.prenom = object.getString("prenom");
        this.pseudo = object.getString("pseudo");
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }
}
