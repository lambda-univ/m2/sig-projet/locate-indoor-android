package com.example.locate_indoor.objets;

import com.mapbox.geojson.Geometry;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Polygon;

import org.json.JSONException;
import org.json.JSONObject;

public class Chemin {
    private int id;
    private Geometry geom;
    private Etage etage_x;
    private Etage etage_y;

    public Chemin(int id, Geometry geom, Etage etage_x, Etage etage_y) {
        this.id = id;
        this.geom = geom;
        this.etage_x = etage_x;
        this.etage_y = etage_y;
    }

    public Chemin() {
    }

    public Chemin(JSONObject object) throws JSONException {
        this.id = object.getInt("id");
        this.geom = LineString.fromJson(String.valueOf(object.getJSONObject("geom")));
        this.etage_x = new Etage(object.getJSONObject("etage_x"));
        this.etage_y = new Etage(object.getJSONObject("etage_y"));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Geometry getGeom() {
        return geom;
    }

    public void setGeom(Geometry geom) {
        this.geom = geom;
    }

    public Etage getEtage_x() {
        return etage_x;
    }

    public void setEtage_x(Etage etage_x) {
        this.etage_x = etage_x;
    }

    public Etage getEtage_y() {
        return etage_y;
    }

    public void setEtage_y(Etage etage_y) {
        this.etage_y = etage_y;
    }
}
