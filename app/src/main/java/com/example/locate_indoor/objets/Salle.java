package com.example.locate_indoor.objets;


import com.mapbox.geojson.Geometry;
import com.mapbox.geojson.Polygon;

import org.json.JSONException;
import org.json.JSONObject;

public class Salle {
    private int id;
    private Geometry geom;
    private String nom;
    private int numero;
    private Etage etage;
    private Categorie categorie;

    public Salle(int id, Geometry geom, String nom, int numero, Etage etage, Categorie categorie) {
        this.id = id;
        this.geom = geom;
        this.nom = nom;
        this.numero = numero;
        this.etage = etage;
        this.categorie = categorie;
    }

    public Salle(JSONObject object) throws JSONException {
        this.id = object.getInt("id");
        this.geom = Polygon.fromJson(String.valueOf(object.getJSONObject("geom")));
        this.nom = object.getString("nom");
        this.numero = object.getInt("numero");
        this.etage = new Etage(object.getJSONObject("etage"));
        this.categorie = new Categorie(object.getJSONObject("categorie"));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Geometry getGeom() {
        return geom;
    }

    public void setGeom(Geometry geom) {
        this.geom = geom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Etage getEtage() {
        return etage;
    }

    public void setEtage(Etage etage) {
        this.etage = etage;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    @Override
    public String toString() {
        return "Salle{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", numero=" + numero +
                '}';
    }
}
