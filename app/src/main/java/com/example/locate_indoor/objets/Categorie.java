package com.example.locate_indoor.objets;

import org.json.JSONException;
import org.json.JSONObject;

public class Categorie {
    private int id;
    private String nom;
    private String couleur;

    public Categorie(int id, String nom, String couleur) {
        this.id = id;
        this.nom = nom;
        this.couleur = couleur;
    }

    public Categorie() {
    }

    public Categorie(JSONObject object) throws JSONException {
        this.id = object.getInt("id");
        this.nom = object.getString("nom");
        this.couleur = object.getString("couleur");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }
                              @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", couleur='" + couleur + '\'' +
                '}';
    }
}
