package com.example.locate_indoor.objets;


import com.mapbox.geojson.Point;

import org.json.JSONException;
import org.json.JSONObject;

public class QrCode {
    private int id;
    private Point point;
    private Etage etage;

    public QrCode(int id, Point point, Etage etage) {
        this.id = id;
        this.point = point;
        this.etage = etage;
    }

    public QrCode() {
    }

    public QrCode(JSONObject object) throws JSONException {
        this.id = object.getInt("id");
        this.point = Point.fromJson(String.valueOf(object.getJSONObject("point")));
        this.etage = new Etage(object.getJSONObject("etage"));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public Etage getEtage() {
        return etage;
    }

    public void setEtage(Etage etage) {
        this.etage = etage;
    }

    @Override
    public String toString() {
        return "QrCode{" +
                "id=" + id +
                ", point=" + point +
                ", etage=" + etage +
                '}';
    }
}
