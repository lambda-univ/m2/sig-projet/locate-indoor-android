package com.example.locate_indoor.objets;

import org.json.JSONException;
import org.json.JSONObject;

public class Historique {
    private String date;
    private String lieu;

    public Historique(String date, String lieu) {
        this.date = date;
        this.lieu = lieu;
    }

    public Historique() {
    }

    public Historique(JSONObject object) throws JSONException {
        this.date = object.getString("date");
        this.lieu = object.getString("nom");
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }
}
