package com.example.locate_indoor.objets;


import com.mapbox.geojson.Geometry;
import com.mapbox.geojson.Polygon;

import org.json.JSONException;
import org.json.JSONObject;

public class Etage {
    private int id;
    private int num;
    private Geometry geom;
    private String nom;

    public Etage(int id, int num, Geometry geom, String nom) {
        this.id = id;
        this.num = num;
        this.geom = geom;
        this.nom = nom;
    }

    public Etage() {
    }

    public Etage(JSONObject object) throws JSONException {
        this.id = object.getInt("id");
        this.num = object.getInt("num");

        try {
            this.geom = Polygon.fromJson(String.valueOf(object.getJSONObject("geom")));
        } catch (JSONException e) {
            this.geom = null;
        }
        this.nom = object.getString("nom");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Geometry getGeom() {
        return geom;
    }

    public void setGeom(Geometry geom) {
        this.geom = geom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Etage{" +
                "id=" + id +
                ", num=" + num +
                ", geom=" + geom +
                ", nom='" + nom + '\'' +
                '}';
    }
}
