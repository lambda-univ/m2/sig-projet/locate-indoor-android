 styles = [
  new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      width: 3,
    }),
    fill: new ol.style.Fill({
      color: 'rgba(0, 0, 255, 0.1)',
    }),
  }),

  new ol.style.Style({
    image: new ol.style.Circle({
      radius: 5,
      fill: new ol.style.Fill({
        color: 'orange',
      }),
    }),
    geometry: function (feature) {
      // return the coordinates of the first ring of the polygon
      var coordinates = feature.getGeometry().getCoordinates()[0];
      return new ol.geom.MultiPoint(coordinates);
    },
  })
  ]

var geojsonObject = {
  'type': 'FeatureCollection',
  'crs': {
    'type': 'name',
    'properties': {
      'name': 'EPSG:4326',
    },
  },
  'features': [
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [-5e6, 6e6],
            [-5e6, 8e6],
            [-3e6, 8e6],
            [-3e6, 6e6],
            [-5e6, 6e6] ] ],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [-2e6, 6e6],
            [-2e6, 8e6],
            [0, 8e6],
            [0, 6e6],
            [-2e6, 6e6] ] ],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [0, 0],
            [10, 0],
            [10, -10],
            [0,-10],
            [0, 0] ] ],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [1e6, 6e6],
            [1e6, 8e6],
            [3e6, 8e6],
            [3e6, 6e6],
            [1e6, 6e6] ] ],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [-2e6, -1e6],
            [-1e6, 1e6],
            [0, -1e6],
            [-2e6, -1e6] ] ],
      },
    } ],
};


var geojsonObject = {
  'type': 'FeatureCollection',
  'crs': {
    'type': 'name',
    'properties': {
      'name': 'EPSG:4326',
    },
  },
  'features': [
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [0, 0],
            [10, 0],
            [10, -10],
            [0,-10],
            [0, 0]
          ]
        ],
      },
    },
   ],
};

console.log("Valid Data: ", geojsonObject)
if ( false ) {
    display(geojsonObject)
}

if ( true ) {
    $.ajax({
          url: 'http://192.168.1.59:8080/salle',
          dataType: 'json',
          type:'get',
          headers: {
              "charset":"UTF-8",
              "accept": "application/json",
              "Access-Control-Allow-Origin":"*",
              "Access-Control-Allow-Credentials":"true",
          },
          success: function(data) {

            console.log("Data:", data)
            geoms = []
            for ( let salleIndex = 0; salleIndex < data.length; salleIndex++ ) {
                salle = data[salleIndex]
                if ( salle.etage.num == 1 ) {
                    for ( let coordIndex = 0; coordIndex < salle.geom.coordinates[0].length; coordIndex ++ ) {
                       coord = salle.geom.coordinates[0][coordIndex]
                       coord[1] += 20

                    }

                }
                geoms.push(salle.geom)
            }

            /*if ( false ) {
                data = data.forEach( salle => {
                    if ( salle.etage.num == 1 ) {
                        coordinates = []
                        salle.geom.coordinates[0].forEach( coord => {
                            coordinates.push([coord[0], coord[1] + 100])
                        })
                        salle.geom.coordinates = coordinates
                    }
                    geoms.push(salle.geom)
                } )
            }*/


            data = geoms
            console.log("Pre-processed data: ", data)

              // Coordonnées -> Features
              features = []

              data.forEach(row => features.push({
                type: 'Feature',
                geometry: {
                    type: row.type,
                    coordinates: row.coordinates
                }
              }))



              // Features -> FeatureCollection
              var geojsonObject = {
                  'type': 'FeatureCollection',
                    'crs': {
                      'type': 'name',
                      'properties': {
                        'name': 'EPSG:4326',
                      },
                    },
                  'features': features
                }
                console.log("Own Data:", geojsonObject)

                display(geojsonObject)






          }.bind(this),
          error: function(xhr, status, err) {}.bind(this)
        });
}



function display(geojsonObject) {


    console.log("Displaying ", geojsonObject)

      var source = new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojsonObject),
      });

      var layer = new ol.layer.Vector({
        source: source,
        style: styles,
      });

      var map = new ol.Map({
        layers: [layer],
        target: 'map',
        view: new ol.View({
          center: [0, 0],
          zoom: 2,
          zoomFactor: 100,
        }),
      });
}