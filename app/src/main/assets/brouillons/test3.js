/**
 * File: script.js
 * Description: Script d'affichage d'OpenLayers
 * Author: Ephemere
 */


////////////////////////////////////////////////////////////////////////////////////////////////////
// Styles functions
////////////////////////////////////////////////////////////////////////////////////////////////////
var styles = {
  'Point': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'yellow',
      width: 1,
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 0, 0.1)',
    }),
  }),
  'LineString': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'green',
      width: 1,
    }),
  }),
  'MultiLineString': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'green',
      width: 1,
    }),
  }),
  /*'MultiPoint': new ol.style.Style({
    image: image,
  }),*/
  'MultiPolygon': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'yellow',
      width: 1,
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 0, 0.1)',
    }),
  }),
  'Polygon': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      lineDash: [4],
      width: 3,
    }),
    fill: new ol.style.Fill({
      color: 'rgba(0, 0, 255, 0.1)',
    }),
  }),
  'Qrcode': new ol.style.Style({
      stroke: new ol.style.Stroke({
        color: 'red',
        lineDash: [4],
        width: 4,
      }),
      fill: new ol.style.Fill({
        color: 'rgba(0, 0, 255, 0.1)',
      }),
    }),
  'GeometryCollection': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'magenta',
      width: 2,
    }),
    fill: new ol.style.Fill({
      color: 'magenta',
    }),
    image: new ol.style.Circle({
      radius: 10,
      fill: null,
      stroke: new ol.style.Stroke({
        color: 'magenta',
      }),
    }),
  }),
  'Circle': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'red',
      width: 2,
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255,0,0,0.2)',
    }),
  }),
};

// Style function
var styleFunction = function (feature) {
  /*console.log("Drawing feature:", feature.getProperties()['prop'])
  console.log("Drawing feature:", feature.getGeometry().getType())*/

  if(feature.getProperties()['prop'] === undefined )
    return styles[feature.getGeometry().getType()];
  return styles["Qrcode"]
};

// global shift
const globalShift = 20


function addFeatureCollectionHeader (features) {
    return {
               'type': 'FeatureCollection',
               'crs': {
                   'type': 'name',
                   'properties': {
                       'name': 'EPSG:4326',
                   },
               },
               'features': features
            }
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Geometries management functions
////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Displays a GeoGSON object in an OpenLayer map
 */
function display(geojsonObject) {
    console.log("Displaying ", geojsonObject)

    // build data source which contains data to draw
    var source = new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojsonObject),
    });

    // build layer object and map to display layer
    var layer = new ol.layer.Vector({
        source: source,
        style: styleFunction,
    });

    var map = new ol.Map({
        layers: [layer],
        target: 'map',
        view: new ol.View({
            center: [0, 0],
            zoom: 2,
            zoomFactor: 100,
        }),
    });
}

/**
 * Returns geometries from a rooms list.
 */
function getGeometryFromSalle(data)
{
    var geoms = []
    //console.log("GetGeometryFromSalle: data: ", data)

    data.forEach( salle => {
        if ( salle.etage.num == 1 )
            salle.geom.coordinates[0].forEach( coords => coords[1] += globalShift)

        geoms.push(salle.geom)
    })
    return geoms
}

/**
 * Returns geometris from stage list.
 */
function getGeometryFromEtage(data)
{
    var geoms = []

    data.forEach( etage => {
        if(etage.num == 1)
            etage.geom.coordinates[0].forEach( coords => coords[1] += globalShift)

        geoms.push(etage.geom)
    })

    return geoms
}


/**
 * Returns geometries from path.
 */
function getGeometryFromChemin(data)
{
    var geoms = []

    data.forEach( chemin => {
        x = chemin.etage_x.num
        y = chemin.etage_y.num
        coords = chemin.geom.coordinates

        if(x == 1) { coords[0][1] += globalShift }
        if(y == 1) { coords[1][1] += globalShift }

        geoms.push(chemin.geom)
    })

    return geoms
}

/**
 * Returns geometries from QRCode list.
 */
function getGeometryFromQrcode(data)
{

    shift = [[0,0],[0,0.1],[0.1,0],[0.1,0.1],[0,0]]

    shift.forEach( c => { if (data.etage.num == 1) { c[1] += globalShift } })

    polygonCoords = []

    shift.forEach( s => {
        coord = data.point.coordinates

        polygonCoords.push( [ coord[0] + s[0] , coord[1] + s[1] ] )
    })

    data.point.type = "Polygon"
    data.point.coordinates = [polygonCoords]

    console.log("QRCODE : ",JSON.stringify(data))
    return [data.point]
}


/**
 * Creates a features collection from a geometries list.
 */
function getFeatureCollectionFromGeomsList(geom_list,props)
{
    features = []

    if(props === undefined){
        geom_list.forEach(row =>
            features.push({
                type: 'Feature',
                geometry: {
                    type: row.type,
                    coordinates: row.coordinates
                }
            }))
    }else{
        //console.log("TEST : ", geom_list.length)
        geom_list.forEach(row =>
            features.push({
                type : 'Feature',
                geometry : {
                     type : row.type,
                     coordinates : row.coordinates
                },
                properties : props
            }))
    }

     // Features -> FeatureCollection
     var featureCollection = addFeatureCollectionHeader(features)

     return featureCollection
}

/**
 * Return Json response for a given request
 */
function computeQuery(queryUrl,queryType,queryData,queryAuth)
{
    return $.ajax({
           url: queryUrl,
           dataType: 'json',
           type: queryType,
           async : false,
           data : JSON.stringify(queryData),
           headers: {
               "charset":"UTF-8",
               "accept": "application/json",
               "Access-Control-Allow-Origin":"*",
               "Access-Control-Allow-Credentials":"true",
               "Content-Type" : "application/json;charset=utf-8",
               "Authorization" : queryAuth,
           },
           success: function(data){  /*console.log("Data:", data)*/ }.bind(this),
           error: function(xhr, status, err) {
               console.info("Request failure on", status, "with", err)
           }.bind(this)
      }).responseJSON;
}

/**
 * Loads data from a given url.
 */
function getFeatureCollectionFromData(queryUrl,queryType,queryData,parser,properties)
{
       console.log("Requesting data from ", queryType, " ", queryUrl," with ", queryData )
       var data =  computeQuery(queryUrl,queryType,queryData)

       console.log("Received data: ", JSON.stringify(data))
       data = parser(data)
       //console.log("Pre-processed data: ", data)

       var geojsonObject = getFeatureCollectionFromGeomsList(data,properties)
       console.log("Own Data:", geojsonObject)

       return  geojsonObject
}


/**
 * Returns a concatenation of GeoJSON.
 */
function concatenationGeoJsonCollection(geojsonCollection)
{
    features = []

    geojsonCollection.forEach( geojson => features = features.concat(geojson.features) )

    return addFeatureCollectionHeader(features)
}

function loadDataFromUrl( method, url, params, handler ) {
    return getFeatureCollectionFromData( url, method, params, handler )
}

///////////////////////////////////////////////////////////////////////////////////////////////////
// Main
///////////////////////////////////////////////////////////////////////////////////////////////////
request = Android.request()
console.log("Init with : ", request)
request = JSON.parse(request)


action = request.action
urls = request.urls
console.log("Action: ", request.action)
console.log("Urls: ", request.urls)


// computes geometries
geoms = []

salleRequest = urls.all_salle
res = getFeatureCollectionFromData(salleRequest.url, salleRequest.method, undefined,getGeometryFromSalle, undefined)
geoms.push(res)


etageRequest = urls.all_etage
res1 = getFeatureCollectionFromData(etageRequest.url, etageRequest.method, undefined, getGeometryFromEtage, undefined)
geoms.push( res1 )

var p = { "prop" : "qrcode" }
currentQrcodeRequest = urls.current_qrcode
res2 = getFeatureCollectionFromData(currentQrcodeRequest.url,currentQrcodeRequest.method,undefined,getGeometryFromQrcode,p)
geoms.push( res2 )


if ( action == "display-path-action" ) {
    itineraireRequest = urls.itineraire
    res3 = getFeatureCollectionFromData(itineraireRequest.url, itineraireRequest.method,itineraireRequest.params,getGeometryFromChemin,undefined)
    geoms.push(res3)
}
d = concatenationGeoJsonCollection( geoms )

console.log("End : ", JSON.stringify(d))
display(d)