if( typeof Android != 'undefined' ) {
    request = Android.request()
    console.log("Init with : ", request)
    request = JSON.parse(request)

    console.log("Action: ", request.action)
    console.log("Urls: ", request.urls)
    console.log("Params: ", request.params)
} else {
    console.log("No Android environement specified")
}

var styles = {
  'Point': new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'red', width: 2, }), fill: new ol.style.Fill({ color: 'red', }), radius : 6,}),
  'LineString': new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'green', width: 2, }), }),
  'MultiLineString': new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'green', width: 1, }), }),
  'MultiPoint': new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'yellow', width: 1, }), fill: new ol.style.Fill({ color: 'rgba(255, 255, 0, 0.1)', }), }),
  'MultiPolygon': new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'yellow', width: 1, }), fill: new ol.style.Fill({ color: 'rgba(255, 255, 0, 0.1)', }), }),
  'Polygon': new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'blue', lineDash: [4], width: 3, }), fill: new ol.style.Fill({ color: 'rgba(0, 0, 255, 0.1)', }), }),
  'GeometryCollection': new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'magenta', width: 2, }), fill: new ol.style.Fill({ color: 'magenta', }),
                        image: new ol.style.Circle({ radius: 10, fill: null, stroke: new ol.style.Stroke({ color: 'magenta', }), }), }),
  'Circle': new ol.style.Style({ stroke: new ol.style.Stroke({ color: 'red', width: 2, }), fill: new ol.style.Fill({ color: 'rgba(255,0,0,0.2)', }), }), };

// Style function
var styleFunction = function (feature)
{
    //console.log("Drawing feature:", feature)
  return styles[feature.getGeometry().getType()];
};

function display(geojsonObject) {

    console.log("Displaying ", geojsonObject)


      var source = new ol.source.Vector({
        features: new ol.format.GeoJSON().readFeatures(geojsonObject),
      });

      var layer = new ol.layer.Vector({
        source: source,
        style: styleFunction,
      });

      var map = new ol.Map({
        layers: [layer],
        target: 'map',
        view: new ol.View({
          center: [0, 0],
          zoom: 3,
          zoomFactor: 100,
        }),
      });
}

function getGeometryFromSalle(data)
{
    var geoms = []

    data.forEach( salle => {
        if ( salle.etage.num == 1 )
            salle.geom.coordinates[0].forEach( coords => coords[1] += 20)

        geoms.push(salle.geom)
    })
    return geoms
}

function getGeometryFromEtage(data)
{
    var geoms = []

    data.forEach( etage => {
        if(etage.num == 1)
            etage.geom.coordinates[0].forEach( coords => coords[1] += 20 )

        geoms.push(etage.geom)
    })

    return geoms
}

function getGeometryFromChemin(data)
{
    var geoms = []

    data.forEach( chemin => {
        if(chemin.etage_x.num == 1 && chemin.etage_y.num == 1)
            chemin.geom.coordinates.forEach( coords => coords[1] += 20 )

        else if(chemin.etage_x.num == 1 && chemin.etage_y.num == 0)
            chemin.geom.coordinates[0][1] += 20

        else if(chemin.etage_x.num == 0 && chemin.etage_y.num == 1)
            chemin.geom.coordinates[1][1] += 20

        geoms.push(chemin.geom)
    })

    return geoms
}

function getGeometryFromQrcode(data)
{
    if(data.etage.num == 1)
        data.point.coordinates.forEach( coords => coords[1] += 20 )

    return [data.point]
}

function getFeatureCollectionFromGeomsList(geom_list)
{
    features = []

    geom_list.forEach(row => features.push({
        type: 'Feature',
        geometry: {
            type: row.type,
            coordinates: row.coordinates
        }
    }))

     // Features -> FeatureCollection
     var featureCollection = {
        'type': 'FeatureCollection',
            'crs': {
                'type': 'name',
                'properties': {
                    'name': 'EPSG:4326',
                },
            },
            'features': features
     }

     return featureCollection
}

function getFeatureCollectionFromData(queryUrl,queryType,queryData,parser)
{
       console.log("Requesting data from ", queryType, " ", queryUrl," with ", queryData )
       var data =  $.ajax({
            url: queryUrl,
            dataType: 'json',
            type: queryType,
            async : false,
            data : JSON.stringify(queryData),
            headers: {
                "charset":"UTF-8",
                "accept": "application/json",
                "Access-Control-Allow-Origin":"*",
                "Access-Control-Allow-Credentials":"true",
                "Content-Type" : "application/json;charset=utf-8",
            },
            success: function(data){  console.log("Data:", data) }.bind(this),
            error: function(xhr, status, err) {
                console.info("Request failure on", status, "with", err)
            }.bind(this)
       }).responseJSON;

       console.log("Received data: ", data)
       data = parser(data)
       console.log("Pre-processed data: ", data)

       var geojsonObject = getFeatureCollectionFromGeomsList(data)
       console.log("Own Data:", geojsonObject)

       return  geojsonObject
}

function concatenationGeoJsonCollection(geojsonCollection)
{
    features = []

    geojsonCollection.forEach( geojson => features = features.concat(geojson.features) )

    return {
       'type': 'FeatureCollection',
           'crs': {
               'type': 'name',
               'properties': {
                   'name': 'EPSG:4326',
               },
           },
           'features': features
    }
}

var t =
{
    "salleId" : 10,
    "qrcodeId" : 2
}

var x = {
            "type": "FeatureCollection",
            "features": [
                {
                    "type": "Feature",
                    "geometry": {
                                   "type": "Point",
                                   "coordinates": [
                                     [1.0, 0.0],
                                     [1.0, 1.0]
                                   ]
                                 }
                }
            ]
        }

res = getFeatureCollectionFromData('http://192.168.1.59:8080/salle','get',undefined,getGeometryFromSalle)
res1 = getFeatureCollectionFromData('http://192.168.1.59:8080/etage','get',undefined,getGeometryFromEtage)
res2 = getFeatureCollectionFromData('http://192.168.1.59:8080/itineraire','post',t,getGeometryFromChemin)
res3 = getFeatureCollectionFromData('http://192.168.1.59:8080/qrcode/2','get',undefined,getGeometryFromQrcode)
console.log("Test : ",res)
console.log("Test1 : ",res1)
console.log("Test2 : ",res2)
console.log("Test3 : ",res3)
d = concatenationGeoJsonCollection([res,res1,res2,res3,x])
display(d)
//display(concatenationGeoJsonCollection([res3,x]))