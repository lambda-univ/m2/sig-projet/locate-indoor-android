  var image = new ol.style.Circle({
      radius: 5,
      fill: null,
      stroke: new ol.style.Stroke({color: 'red', width: 1}),
  });

// Defines styles
var styles = {
  'Point': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'yellow',
      width: 1,
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 0, 0.1)',
    }),
  }),
  'LineString': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'green',
      width: 1,
    }),
  }),
  'MultiLineString': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'green',
      width: 1,
    }),
  }),
  'MultiPoint': new ol.style.Style({
    image: image,
  }),
  'MultiPolygon': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'yellow',
      width: 1,
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 0, 0.1)',
    }),
  }),
  'Polygon': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'blue',
      lineDash: [4],
      width: 3,
    }),
    fill: new ol.style.Fill({
      color: 'rgba(0, 0, 255, 0.1)',
    }),
  }),
  'GeometryCollection': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'magenta',
      width: 2,
    }),
    fill: new ol.style.Fill({
      color: 'magenta',
    }),
    image: new ol.style.Circle({
      radius: 10,
      fill: null,
      stroke: new ol.style.Stroke({
        color: 'magenta',
      }),
    }),
  }),
  'Circle': new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'red',
      width: 2,
    }),
    fill: new ol.style.Fill({
      color: 'rgba(255,0,0,0.2)',
    }),
  }),
};


// Style function
var styleFunction = function (feature) {
    console.log("Drawing feature:", feature)
  return styles[feature.getGeometry().getType()];
};

// data object
var test = {
  'type': 'FeatureCollection',
  'crs': {
    'type': 'name',
    'properties': {
      'name': 'EPSG:3857',
    },
  },
  'features': [
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [0, 0],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'LineString',
        'coordinates': [
          [4e6, -2e6],
          [8e6, 2e6]
        ],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
            [
                [-5e6, -1e6],
                [-4e6, 1e6],
                [-3e6, -1e6]
            ]
        ],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'MultiLineString',
        'coordinates': [
          [
            [-1e6, -7.5e5],
            [-1e6, 7.5e5] ],
          [
            [1e6, -7.5e5],
            [1e6, 7.5e5] ],
          [
            [-7.5e5, -1e6],
            [7.5e5, -1e6] ],
          [
            [-7.5e5, 1e6],
            [7.5e5, 1e6] ] ],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'MultiPolygon',
        'coordinates': [
          [
            [
              [-5e6, 6e6],
              [-5e6, 8e6],
              [-3e6, 8e6],
              [-3e6, 6e6] ] ],
          [
            [
              [-2e6, 6e6],
              [-2e6, 8e6],
              [0, 8e6],
              [0, 6e6] ] ],
          [
            [
              [1e6, 6e6],
              [1e6, 8e6],
              [3e6, 8e6],
              [3e6, 6e6] ] ] ],
      },
    }],
};



geojson = new ol.format.GeoJSON()
console.log("GSON: ", geojsonObject)
       // Vector source
         var vectorSource = new ol.source.Vector({
            format: geojson.readFeatures(test),
            projection: 'EPSG:4326',
         })

         // Vector layer

        var restaurantsold = new ol.layer.Vector({
            title: 'test',
            source : vectorSource,
            style: (feature) => {
                console.log(feature);
                styleFunction(feature);
            },
        });



        // Map
        var map = new ol.Map({
          layers: [restaurantsold],
          target: 'map',
          view: new ol.View({
            center: [0, 0],
            zoom: 2,
          }),

        });



$.ajax({
      url: 'http://localhost:8080/geom/salle',
      dataType: 'json',
      type:'get',
      headers: {
          "charset":"UTF-8",
          "accept": "application/json",
          "Access-Control-Allow-Origin":"*",
          "Access-Control-Allow-Credentials":"true",
      },
      success: function(data) {
          // Coordonnées -> Features
          features = [{
                type: 'Feature',
                geometry: {
                    'type': 'Point',
                    'coordinates': [0, 0],
                }
          }]
          console.log(features)

          data.forEach(row => features.push({
            type: 'Feature',
            geometry: row,
          }))

          // Features -> FeatureCollection
          var geojsonObject = {
              'type': 'FeatureCollection',
              'features': features
            }



      }.bind(this),
      error: function(xhr, status, err) {}.bind(this)
    });
