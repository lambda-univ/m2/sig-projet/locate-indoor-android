﻿Application Android Studio de localisation dans un batiment 

Connexion Activity : ~~xml~~ / ~~java~~

    - Permet à l'utilisateur de se connecter

Inscription Activity : ~~xml~~ / ~~java~~

    - Permet à l'utilisateur de s'inscrire

Menu Activity : ~~xml~~ / ~~java~~

    - Permet à l'utilisateur de choisir ce qu'il souhaite faire

Historique Activity : ~~xml~~ / ~~java~~

    - Permet d'afficher les derniers lieux ou se trouvaient les utilisateurs

ModifSalle Activity : ~~xml~~ / ~~java~~

    - Permet de choisir une salle et de modifier son nom.

ScanQR Activity : ~~xml~~ / ~~java~~

    - Permet de scanner un QR code afin de récupérer les cordonnées de l'utilisateur via la camera

Plan Activity : ~~xml~~ / java

    - Affichage du plan du batiment avec position de l'utilisateur
    - Si une destination est entrée -> affichage du chemin + bouton pour désafficher le chemin (annulation de la destination) + bouton arriver a destination
    - Sinon bouton pour entrer une destination + bouton pour actualiser la position (retour au scan)

Destination Activity : ~~xml~~ / ~~java~~

    - Liste cliquable des différentes salles du batiment

Questions :

    - Ajouter un bouton de déconnexion ?
    - Ajouter popUp qui demande les différentes autorisation (internet et camera) ou on trouve comment forcer?